package ru.danik_ik.xsdvalidation;

import picocli.CommandLine.Parameters;

import java.io.File;

public class CliParams {
    @Parameters(index = "0", paramLabel="PATH_TO_XML", description="path of the input FILE(s)")
    private File inputFolder;
    @Parameters(index = "1", paramLabel="PATH_TO_XSD", description="path of the xsd FILE(s)")
    private File xsdFolder;

    public File getInputFolder() {
        return inputFolder;
    }

    public File getXsdFolder() {
        return xsdFolder;
    }
}
