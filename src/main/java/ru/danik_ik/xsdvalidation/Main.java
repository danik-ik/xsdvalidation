package ru.danik_ik.xsdvalidation;

import picocli.CommandLine;

import java.io.File;
import java.util.List;

public class Main
{
    public static void main(String[] args) {

        CliParams params = new CliParams();
        new CommandLine(params).parse(args);

        final File inputFolder;
        final File xsdFolder;

        inputFolder = params.getInputFolder();
        xsdFolder = params.getXsdFolder();

        System.out.println("xml folder: " + inputFolder);
        List<String> xmls = List.of(inputFolder.list((folder, name) -> name.endsWith(".xml")));
        for (String name: xmls) System.out.println("-> " + name);

        System.out.println("xsd folder: " +xsdFolder);

        List<String> xsds = List.of(xsdFolder.list((folder,  name) -> name.endsWith(".xsd")));
        for (String name: xsds) System.out.println("-> " + name);

        boolean allOk = true;

        for (String xmlName: xmls) {
            String xsdName = xmlName.replaceFirst(".xml$", "") + ".xsd";
            if (xsds.contains(xsdName)) {
                boolean StepOk = XmlValidator.validateXMLSchema(
                        inputFolder + File.separator + xmlName,
                        xsdFolder + File.separator + xsdName);
                allOk = allOk && StepOk;
            } else {
                System.out.println("===========================================================");
                System.out.println("ERROR: xsd for " + xmlName + " (" + xsdName + ") not found in xsd folder " + xsdFolder);
            }
        }

        if (!allOk) {
            System.exit(1);
        }
    }

}

