package ru.danik_ik.xsdvalidation;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class XmlValidator {
    public static boolean validateXMLSchema(String xmlPath, String xsdPath){
        System.out.println("===========================================================");
        System.out.println("validating " + xmlPath + " using " + xsdPath);
        try {
            SchemaFactory factory =
                    SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new File(xsdPath));
            Validator validator = schema.newValidator();

            final ExceptionsCollector collector = new ExceptionsCollector();
            validator.setErrorHandler(collector);

            validator.validate(new StreamSource(new File(xmlPath)));

            collector.getErrors().forEach(r -> System.out.println(r.level + ": " + r.exception.getMessage()) );

            boolean allOk = collector.getErrors().isEmpty();
            if (allOk) System.out.println("All OK!");
            return allOk;
        } catch (IOException | SAXException e) {
            System.out.println("Exception: "+e.getMessage());
            return false;
        }
    }
    private static enum ErrorLevel {WARNING,ERROR,FATAL}

    private static class ExceptionRec {
        ErrorLevel level;
        SAXParseException exception;

        public ExceptionRec(ErrorLevel level, SAXParseException exception) {
            this.level = level;
            this.exception = exception;
        }
    }

    private static class ExceptionsCollector implements ErrorHandler {
        private final List<ExceptionRec> list;

        public ExceptionsCollector() {
            this.list = new LinkedList<>();
        }

        public List<ExceptionRec> getErrors() {
            return Collections.unmodifiableList(list);
        }

        @Override
        public void warning(SAXParseException exception) throws SAXException {
            list.add(new ExceptionRec(ErrorLevel.WARNING, exception));
        }

        @Override
        public void error(SAXParseException exception) throws SAXException {
            list.add(new ExceptionRec(ErrorLevel.ERROR, exception));
        }

        @Override
        public void fatalError(SAXParseException exception) throws SAXException {
            list.add(new ExceptionRec(ErrorLevel.FATAL, exception));
        }
    }

}
